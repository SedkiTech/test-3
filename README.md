## Quick run

```bash
git clone git@gitlab.com:achrefbenslimene/test-3.git test-3
cd test-3/
```

Put your name and create new branch

```bash
git checkout -b test/flen-ben-foulen
git push -u
```

```bash
npm install
npm start
```

## Development tricks

Use nanoid to generate ids

```bash
import { nanoid } from "nanoid";
console.log(nanoid(10)); --> qa1gAk2WpZ
```

Sidebar links example

```bash
import { Link } from "react-router-dom";
<Link to="/all-files">All Files</Link>
```