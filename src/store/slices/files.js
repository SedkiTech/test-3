import { createSlice } from "@reduxjs/toolkit";
import FILES from "../../constants/files";
const initialState = {
  files: FILES,
  //initial files sorted by date ascending
  // files: FILES.sort((a, b) => {
  //   return new Date(b.createdAt) - new Date(a.createdAt);
  // }),
};

export const filesSlice = createSlice({
  name: "files",
  initialState,
  reducers: {
    test: (state, action) => {
      state.value += action.payload;
    },

    uploadFile: (state, action) => {
      let newFile = action.payload;
      console.log("newFile:", newFile);
      newFile.map((file) => (state.files = [file, ...state.files]));
      console.log("state.files:", state.files);
    },

    byDate: (state, action) => {
      // const starredPage = action.payload[0].isStarred;
      // console.log(starredPage);

      state.files = state.files.sort((a, b) => {
        return new Date(b.createdAt) - new Date(a.createdAt); //  ascending
      });

      console.log(state.files);
    },
    byName: (state, action) => {
      state.files = state.files.sort((a, b) => {
        return a.name > b.name ? 1 : a.name < b.name ? -1 : 0;
      });

      console.log(state.files);
    },

    bySize: (state, action) => {
      state.files = state.files.sort((a, b) => {
        return b.size > a.size ? 1 : b.size < a.size ? -1 : 0;
      });

      console.log(state.files);
    },

    addStarredFile: (state, action) => {
      console.log(state.files);

      const id = action.payload.id;
      const fileStarred = state.files.find((item) => item.id === id);
      fileStarred.isStarred = !fileStarred.isStarred;
      fileStarred.starredAt = new Date()
        .toISOString(" ")
        .slice(0, 19)
        .replace("T", " ");
    },

    addArchivedFile: (state, action) => {
      const id = action.payload.id;
      const fileArchived = state.files.find((item) => item.id === id);
      fileArchived.isArchived = !fileArchived.isArchived;
      fileArchived.archivedAt = new Date()
        .toISOString(" ")
        .slice(0, 19)
        .replace("T", " ");
    },

    removeStarredFile: (state, action) => {
      console.log(action.payload);
      state.files = state.files.map((file) =>
        file.id === action.payload.id ? { ...file, isStarred: false } : file
      );
      console.log("state.files", state.files);
    },

    removeArchivedFile: (state, action) => {
      console.log(action.payload);

      state.files = state.files.map((item) =>
        item.id === action.payload.id ? { ...item, isArchived: false } : item
      );
      console.log(state.files);
    },
  },
});

//export const { test } = filesSlice.actions;
export const fileActions = filesSlice.actions;

export default filesSlice.reducer;
