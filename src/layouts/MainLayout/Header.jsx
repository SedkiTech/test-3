//import styles

import "./_Header.scss";
//import ICONS
import settingsIcon from "../../assets/icons/Settings.svg";
import bellIcon from "../../assets/icons/NotificationBell.svg";
import userPic from "../../assets/images/user1.png";
import searchIcon from "../../assets/icons/Search.svg";
const Header = () => {
  return (
    <div className="header">
      <div className="search-bar">
        <div className="search">
          <img src={searchIcon} alt="searchIcon" />
        </div>
        <input placeholder="Search" type="text"></input>
      </div>

      <button className="btn settings-btn">
        <img src={settingsIcon} alt="settingsIcon" />
      </button>

      <button className="btn bell-btn">
        <img src={bellIcon} alt="bellIcon" />
        <p>18</p>
      </button>

      <div className="username">Sedki</div>
      <div className="user-picture">
        <img src={userPic} alt="userPic" />
      </div>
    </div>
  );
};

export default Header;
