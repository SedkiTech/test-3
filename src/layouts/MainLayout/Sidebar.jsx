import React from "react";

import { NavLink } from "react-router-dom";
//import styles
import "./_Sidebar.scss";
//import icons
import logo from "../../assets/images/logo.svg";
import uploadIcon from "../../assets/icons/Upload.svg";
import homeIcon from "../../assets/icons/HomeMinimal.svg";
import fileStructureIcon from "../../assets/icons/FileStructure.svg";
import starIcon from "../../assets/icons/Star.svg";
import boxIcon from "../../assets/icons/Box.svg";
//redux
import { useDispatch } from "react-redux";
import files, { fileActions } from "../../store/slices/files";
//id generator
import { nanoid } from "nanoid";
//import maxmaxVolume
import maxVolume from "../../constants/volume";
// import toastify
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
//import guessFileType
import guessFileType from "../../utils/guessFileType";
import FILES from "./../../constants/files";

const Sidebar = () => {
  const dispatch = useDispatch();

  const hiddenFileInput = React.useRef(null);
  const handleClick = (event) => {
    hiddenFileInput.current.click();
  };
  const handleChange = (event) => {
    const fileUploaded = Array.from(event.target.files);
    let videosSize = null;
    let docsSize = null;
    let imgsSize = null;
    console.log("fileUploaded", fileUploaded);

    const newList = [];

    let fileStorageUsed = null;
    let videoStorageUsed = null;
    let imageStorageUsed = null;
    console.log("FILES.length", FILES.length);
    for (let i = 0; i < FILES.length; i++) {
      if (
        FILES[i].type === "png" ||
        FILES[i].type === "gif" ||
        FILES[i].type === "jpg"
      ) {
        imageStorageUsed += FILES[i].size;
      }

      if (
        FILES[i].type === "avi" ||
        FILES[i].type === "mov" ||
        FILES[i].type === "mp4"
      ) {
        videoStorageUsed += FILES[i].size;
      }

      if (
        FILES[i].type === "txt" ||
        FILES[i].type === "pdf" ||
        FILES[i].type === "doc"
      ) {
        fileStorageUsed += FILES[i].size;
      }
    }

    for (let i = 0; i < fileUploaded.length; i++) {
      if (
        guessFileType(fileUploaded[i].type.slice(-3)).elementType ===
        "typeError"
      ) {
        toast.error("Check your files TYPE!");
        console.log("type Error");
      }

      if (
        guessFileType(fileUploaded[i].name.split(".")[1]).elementType === "file"
      ) {
        docsSize += fileUploaded[i].size;
        console.log("docsSize", docsSize);
        docsSize += fileStorageUsed;

        console.log("maxVolume.docs", maxVolume.docs);
        if (docsSize <= maxVolume.docs) {
          console.log("docsSize ok");

          newList.push({
            id: nanoid(10),
            name: fileUploaded[i].name.slice(0, -4),
            type: fileUploaded[i].type.slice(-3),
            size: fileUploaded[i].size,
            createdAt: Date.now(),
            isStarred: false,
            starredAt: null,
            isArchived: false,
            archivedAt: null,
          });
          toast.success("SUCCESS!", {
            theme: "colored",
          });
        } else {
          console.log("docsSize error of limit exceeded");
          toast.error("LIMIT EXCEEDED!  Documents storage is full !", {
            theme: "colored",
          });
        }
      }

      if (
        guessFileType(fileUploaded[i].type.slice(-3)).elementType === "image"
      ) {
        console.log("imageStorageUsed", imageStorageUsed);
        imgsSize += fileUploaded[i].size;
        console.log("imgsSize", imgsSize);

        imgsSize += imageStorageUsed;

        console.log("imgsSize + adding results", imgsSize);
        console.log("maxVolume.imgs", maxVolume.imgs);

        if (imgsSize <= maxVolume.imgs) {
          newList.push({
            id: nanoid(10),
            name: fileUploaded[i].name.slice(0, -4),
            type: fileUploaded[i].type.slice(-3),
            size: fileUploaded[i].size,
            createdAt: Date.now(),
            isStarred: false,
            starredAt: null,
            isArchived: false,
            archivedAt: null,
          });
          toast.success("SUCCESS!", {
            theme: "colored",
          });
        } else {
          console.log("Image error limit exceeded");

          toast.error("LIMIT EXCEEDED! Image storage is full !", {
            theme: "colored",
          });
        }
      }

      if (
        guessFileType(fileUploaded[i].type.slice(-3)).elementType === "video"
      ) {
        videosSize += fileUploaded[i].size;
        videosSize += videoStorageUsed;

        if (videosSize <= maxVolume.videos) {
          console.log("videosSize ok");
          newList.push({
            id: nanoid(10),
            name: fileUploaded[i].name.slice(0, -4),
            type: fileUploaded[i].type.slice(-3),
            size: fileUploaded[i].size,
            createdAt: Date.now(),
            isStarred: false,
            starredAt: null,
            isArchived: false,
            archivedAt: null,
          });
          toast.success("SUCCESS!", {
            theme: "colored",
          });
        } else {
          console.log("videos error limit exceeded");

          toast.error("LIMIT EXCEEDED! Videos storage is full !", {
            theme: "colored",
          });
        }
      }
    }

    dispatch(fileActions.uploadFile(newList));
  };

  return (
    <div className="sidebar">
      <div className="logo">
        <img src={logo} alt="logo" />
      </div>
      <div className="upload-button">
        <button onClick={handleClick}>
          <img src={uploadIcon} alt="uploadIcon" />
          Upload
        </button>
        <input
          multiple
          type="file"
          ref={hiddenFileInput}
          onChange={handleChange}
          style={{ display: "none" }}
        />
      </div>

      <ToastContainer autoClose={8000} />
      <div className="sidebar-list">
        <ul>
          <li>
            <NavLink to="/">
              <img src={homeIcon} alt="homeIcon" />
              Home
            </NavLink>
          </li>
          <li>
            <NavLink to="/all-files">
              <img src={fileStructureIcon} alt="fileStructureIcon" />
              All Files
            </NavLink>
          </li>

          <li>
            <NavLink to="/starred">
              <img src={starIcon} alt="starIcon" />
              Starred
            </NavLink>
          </li>
          <li>
            <NavLink to="/archived">
              <img src={boxIcon} alt="boxIcon" />
              Archived
            </NavLink>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default Sidebar;
