const volume = {
  docs: 104857600,
  imgs: 1073741824,
  videos: 5368709120,
};

export default volume;
