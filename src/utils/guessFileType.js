import imageIcon from "../assets/icons/Image.svg";
import videoIcon from "../assets/icons/Video.svg";
import fileIcon from "../assets/icons/File.svg";

const guessFileType = (fileType) => {
  if (fileType === "avi" || fileType === "mov" || fileType === "mp4") {
    return { type: videoIcon, style: "videoIcon", elementType: "video" };
  } else if (fileType === "txt" || fileType === "pdf" || fileType === "doc") {
    return { type: fileIcon, style: "fileIcon", elementType: "file" };
  } else if (fileType === "png" || fileType === "gif" || fileType === "jpg") {
    return { type: imageIcon, style: "imageIcon", elementType: "image" };
  } else {
    return { elementType: "typeError" };
  }
};

export default guessFileType;
