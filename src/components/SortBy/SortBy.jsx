import React from "react";
// import styles
import "./_index.scss";
//redux
import { useDispatch, useSelector } from "react-redux";
import { fileActions } from "../../store/slices/files";
const SortBy = () => {
  let sortValue = "byDate";
  const dispatch = useDispatch();
  const files = useSelector((state) => state.files.files);

  const sortHandler = (e) => {
    console.log(e.target.value);
    sortValue = e.target.value;

    if (sortValue === "byDate") {
      dispatch(fileActions.byDate(files));
    }

    if (sortValue === "bySize") {
      dispatch(fileActions.bySize(files));
    }
    if (sortValue === "byName") {
      dispatch(fileActions.byName(files));
    }
  };

  return (
    <div className="sort">
      <select onChange={sortHandler} id="files" defaultValue={"byDate"}>
        <option value="byName">By Name</option>
        <option value="bySize">By Size</option>
        <option value="byDate">By Date</option>
      </select>
    </div>
  );
};

export default SortBy;
