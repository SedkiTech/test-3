import React from "react";
//import styles
import "./_StarredHeader.scss";
// import component
import SortBy from "../../components/SortBy/SortBy";
import starIcon from "../../assets/icons/Star.svg";
const StarredHeader = () => {
  return (
    <div className="starredHeader-main">
      <div className="left-container">
        <div className="square">
          <img src={starIcon} alt="starIcon" />
        </div>
        <h1 className="title">Starred Files</h1>
      </div>
      <div className="right-container">
        <SortBy />
      </div>
    </div>
  );
};

export default StarredHeader;
