import React from "react";
//import styles
import "./_StarredFilesTable.scss";

//import ICONS

import trashIcon from "../../assets/icons/Delete.svg";
//import Utils
import bytesToSize from "../../utils/bytesToSize";
import dateToNewDate from "../../utils/dateToNewDate";
import guessFileType from "../../utils/guessFileType";
//redux
import { useDispatch, useSelector } from "react-redux";
import { fileActions } from "../../store/slices/files";

const StarredFilesTable = () => {
  const dispatch = useDispatch();
  const files = useSelector((state) => state.files.files);
  return (
    <div>
      {" "}
      <table id="keywords" cellSpacing="0" cellPadding="0">
        <thead>
          <tr>
            <th>
              <span className="th-name">Name</span>
            </th>
            <th>
              <span className="th-date">Date created</span>
            </th>
            <th>
              <span className="th-size">Size</span>
            </th>
            <th>
              <span className="th-actions ">Actions</span>
            </th>
          </tr>
        </thead>
        <tbody>
          {files.map((file) => {
            return (
              file.isStarred && (
                <tr key={file.id}>
                  <td>
                    <img
                      src={guessFileType(file.type).type}
                      alt=""
                      className={guessFileType(file.type).style}
                    />

                    <span className="file-name">
                      {file.name}.{file.type}
                    </span>
                  </td>
                  <td>{dateToNewDate(file.starredAt)}</td>

                  <td> {bytesToSize(file.size)}</td>
                  <td>
                    <div className="elements-actions">
                      <button
                        className="elements-actions-box"
                        onClick={() => {
                          dispatch(fileActions.removeStarredFile(file));
                        }}
                      >
                        <img src={trashIcon} alt="trashIcon" />
                      </button>
                    </div>
                  </td>
                </tr>
              )
            );
          })}
        </tbody>
      </table>
    </div>
  );
};

export default StarredFilesTable;
