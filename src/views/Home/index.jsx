//import styles
import "./_index.scss";
//import components
import MediaCards from "./MediaCards";
import HomeFilesTable from "./HomeFilesTable";
import RightSidebar from "./RightSidebar";
const Home = () => {
  return (
    <div className="home-main">
      <MediaCards />
      <div className="home-content">
        <HomeFilesTable />
        <RightSidebar />
      </div>
    </div>
  );
};

export default Home;
