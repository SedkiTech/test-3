import React from "react";
//import styles
import "./_mediaCards.scss";
import Styled from "styled-components";
// import ICONS
import fileIcon from "../../assets/icons/File.svg";
import imageIcon from "../../assets/icons/Image.svg";
import videoIcon from "../../assets/icons/Video.svg";
// import utils
import bytesToSize from "../../utils/bytesToSize";
const MediaCards = () => {
  let valueDoc = "4857600";
  let maxDoc = "104857600";
  let backgroundColor = "#ee88";
  let background = "#eee";

  let color = "lightBlue";
  let width = "100%";
  const primary = "#610bef";
  const secondary = "#005bd4";
  const tertiary = "#008a00";

  const Title = Styled.h1`
    font-size: 1.5em;
    text-align: center;
    color: palevioletred;
  `;

  const Container = Styled.div`
      margin-left: 30px;
      margin-right: 30px;

  progress {
    margin-right: 8px;
  }

  progress[value] {
    width: ${(props) => props.width};

    -webkit-appearance: none;
    appearance: none;
  }

  progress[value]::-webkit-progress-bar {
    height: 12px;
    border-radius: 20px;
    background-color: #d9dbe9;
  }  

  progress[value]::-webkit-progress-value {
    height: 12px;
    border-radius: 20px;
    background-color: ${(props) => props.color};
  }
`;

  return (
    <div className="media-container">
      {/* document box */}
      <div className="boxes box-1">
        <div className="icon-box icon-box-document-color">
          <img src={fileIcon} alt="fileIcon" />
        </div>
        <div className="documents_title">Documents</div>

        <Container color={primary} width={width}>
          <progress value={valueDoc} max={maxDoc} />
          <div className="percentage-div">
            <div className="percentage-fixed">
              {" "}
              {((valueDoc / maxDoc) * 100).toFixed(0)}%{" "}
            </div>
            <div className="usage-div">
              {`${bytesToSize(valueDoc)} of ${bytesToSize(maxDoc)} Used`}
            </div>
          </div>
        </Container>

        {/* usage text  */}
      </div>
      {/* image box */}
      <div className="boxes ">
        <div className="icon-box icon-box-image-color ">
          <img src={imageIcon} alt="fileIcon" />
        </div>
        <div className="documents_title">Images</div>
        <div className="progress-bar">
          <div className="progress progress-image-color"></div>
        </div>
        <div className="progress-counter">
          <div className="percentage">29%</div>
          <div className="usage">300mb of 1gb Used</div>
        </div>
      </div>
      {/* video box */}
      <div className="boxes ">
        <div className="icon-box icon-box-video-color">
          <img src={videoIcon} alt="fileIcon" />
        </div>
        <div className="documents_title">Video</div>
        <div className="progress-bar">
          <div className="progress progress-video-color"></div>
        </div>
        <div className="progress-counter">
          <div className="percentage">29%</div>
          <div className="usage">0.6gb of 5gb Used</div>
        </div>
      </div>
    </div>
  );
};

export default MediaCards;
