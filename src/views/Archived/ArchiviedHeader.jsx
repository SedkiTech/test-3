import React from "react";
//import styles
import "./_ArchiviedHeader.scss";
//import icons
import boxIcon from "../../assets/icons/Box.svg";
import SortBy from "../../components/SortBy/SortBy";
const ArchiviedHeader = () => {
  return (
    <div className="main">
      <div className="left-container">
        <div className="square">
          <img src={boxIcon} alt="boxIcon" />
        </div>
        <h1 className="title">Archived Files</h1>
      </div>
      <div className="right-container">
        <SortBy />
      </div>
    </div>
  );
};

export default ArchiviedHeader;
