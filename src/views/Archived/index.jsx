//import styles
import ArchivedFilesTable from "./ArchivedFilesTable";
import ArchiviedHeader from "./ArchiviedHeader";
import "./_index.scss";

const Archived = () => {
  return (
    <div className="archived">
      {/* Header */}
      <ArchiviedHeader />
      {/* table */}
      <ArchivedFilesTable />
    </div>
  );
};

export default Archived;
